﻿using System;
using System . Collections . Generic;
using System . Linq;
using System . Text;
using System . Threading . Tasks;

namespace DABPAK {
	class Program {
		static void Main( string[] args ) {
			if (args.Length < 2) {
				Console . WriteLine ( "USAGE:\r\n" +
					"DABPAK [/?][/I || DAB][/S [CLP]][/R || REMOVE][/F || FETCH][/U || UPDATE]\r\n\r\n" +
					"/? - Show this message\r\n" +
					"/I or DAB - Download and install a package in default %PATH%ed directory specified in DABPAK configuration\r\n" +
					"/S - Start DABbed package right after it's downloaded, optionally passing command-line parameters, for example - /S /? to start package with /? passed\r\n" +
					"/R or REMOVE - Remove the specified package\r\n" +
					"/F or FETCH - Download package source code (if available) to current directory\r\n" +
					"/U or UPDATE - Update the specified package\r\n" +
					"/DAB - Dab\r\n\r\n" +
					"Example:\r\n" +
					"DABPAK /I quizhouse_poor\r\nOR\r\n" +
					"DABPAK DAB quizhouse_poor" );

			}
		}
	}
}
